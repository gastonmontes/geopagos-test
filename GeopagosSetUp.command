#!/bin/sh

cd $(dirname $0)

checkStatus() {
	if [[ $? != 0 ]]; then
		echo $1
		exit 1
	fi

	echo "$2"
}

git clone https://gastonmontes@bitbucket.org/gastonmontes/geopagos-test.git --verbose
checkStatus "############################## - FALLA AL CLONAR EL REPOSITORIO - ##############################" "############################## - REPOSITORIO CLONADO CORRECTAMENTE - ##############################"

cd geopagos-test
checkStatus "############################## - NO SE ENCUENTRA LA CARPETA DEL REPOSITORIO CLONADO - ##############################" "############################## - CARPETA DEL REPOSITORIO ENCONTRADA - ##############################"

cd GeopagosTest
checkStatus "############################## - NO SE ENCUENTRA LA CARPETA DEL REPOSITORIO CLONADO - ##############################" "############################## - CARPETA DEL REPOSITORIO ENCONTRADA - ##############################"

git checkout master
checkStatus "############################## - NO SE PUDO CHECKOUTEAR EL BRANCH MASTER - ##############################" "############################## - BRANCH MASTER CHECKOUTEADO CORRECTAMENTE - ##############################"

pod install --verbose
checkStatus "############################## - ERROR AL INSTALAR COCOAPODS - ##############################" "############################## - COCOAPODS INSTALADO CON EXITO - ##############################"

open GeopagosTest.xcworkspace
checkStatus "############################## - ERROR AL ABRIR EL WORKSPACE DE XCODE - ##############################" "############################## - WORKSPACE ABIERTO CORRECTAMENTE - ##############################"

exit(0)