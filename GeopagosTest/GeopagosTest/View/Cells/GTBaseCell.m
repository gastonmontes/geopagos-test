//
//  GTPaymentMethodCell.m
//  GeopagosTest
//
//  Created by Gaston  Montes on 22/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import "GTBaseCellViewModel.h"
#import "GTBaseCell.h"

#import "UIColor+Geopagos.h"
#import "UIImageView+URL.h"
#import "NSString+URL.h"

static NSString *const kBaseCellIdenfitier = @"baseCellIdentifier";

@interface GTBaseCell ()

@property (nonatomic, weak) IBOutlet UIImageView *paymentMethodImageView;
@property (nonatomic, weak) IBOutlet UILabel *paymentMethodNameLabel;
@property (nonatomic, weak) IBOutlet UIView *cellSeparatorView;

@property (nonatomic, strong) IBOutletCollection(NSLayoutConstraint) NSArray *paymentMethodImageHorizontalConstraints;

@end

@implementation GTBaseCell

#pragma mark - View life cycle.
- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self setupNameLabel];
}

#pragma mark - Styling functions.
- (void)setupNameLabel {
    self.paymentMethodNameLabel.textColor = [UIColor colorTextLight];
}

- (void)setupCellSeparatorView {
    self.cellSeparatorView.backgroundColor = [UIColor colorTextLight];
}

#pragma mark - Setter functions.
- (void)cellSetViewModel:(GTBaseCellViewModel *)viewModel {
    self.paymentMethodNameLabel.text = viewModel.paymentMethodCellName;
    
    if (viewModel.paymentMethodCellImageURLString.length > 0) {
        [self.paymentMethodImageView imageViewSetImageFromURL:viewModel.paymentMethodCellImageURLString
                                                      success:^(UIImage *image) {}
                                                         fail:^(NSError *error) {}];
    } else {
        for (NSLayoutConstraint *constraint in self.paymentMethodImageHorizontalConstraints) {
            constraint.constant = 0;
        }
    }
}

#pragma mark - Reuse cell funtions
+ (NSString *)cellIdentifier {
    return kBaseCellIdenfitier;
}

+ (NSString *)cellNibName {
    return NSStringFromClass([GTBaseCell class]);
}

@end
