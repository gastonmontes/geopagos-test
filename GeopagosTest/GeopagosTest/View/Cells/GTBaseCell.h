//
//  GTPaymentMethodCell.h
//  GeopagosTest
//
//  Created by Gaston  Montes on 22/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GTBaseCellViewModel;

NS_ASSUME_NONNULL_BEGIN

@interface GTBaseCell : UITableViewCell

- (void)cellSetViewModel:(GTBaseCellViewModel *)viewModel;

+ (NSString *)cellIdentifier;
+ (NSString *)cellNibName;

@end

NS_ASSUME_NONNULL_END
