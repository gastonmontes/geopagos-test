//
//  GTPaymentMethodSectionHeader.h
//  GeopagosTest
//
//  Created by Gaston  Montes on 23/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GTBaseSectionHeader : UIView

+ (instancetype)createHeaderWithTitle:(NSString *)headerTitle;
+ (CGFloat)headerHeight;

@end

NS_ASSUME_NONNULL_END
