//
//  GTPaymentMethodSectionHeader.m
//  GeopagosTest
//
//  Created by Gaston  Montes on 23/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import "GTBaseSectionHeader.h"

#import "UIColor+Geopagos.h"
#import "UIView+Nibs.h"

@interface GTBaseSectionHeader ()

@property (nonatomic, weak) IBOutlet UILabel *headerTitleLabel;

@end

@implementation GTBaseSectionHeader

#pragma mark - View creation.
+ (instancetype)createHeaderWithTitle:(NSString *)headerTitle {
    GTBaseSectionHeader *header = [GTBaseSectionHeader viewLoadFromNibName:NSStringFromClass([GTBaseSectionHeader class])];
    header.backgroundColor = [UIColor colorMainBlue];
    header.headerTitleLabel.text = headerTitle;
    header.headerTitleLabel.textColor = [UIColor colorMainWhite];
    return header;
}

#pragma mark - Size functions.
+ (CGFloat)headerHeight {
    return 40.0f;
}

@end
