//
//  GTLoadingView.m
//  GeopagosTest
//
//  Created by Gaston  Montes on 17/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import "GTLoadingView.h"

#import "UIColor+Geopagos.h"
#import "UIView+Nibs.h"

static NSInteger const kLoadingViewTag = 56432;
static CGFloat const kLoadingViewAlpha = 1.0;
static NSString *const kLoadingViewNibName = @"GTLoadingView";

@implementation GTLoadingView

#pragma mark - View creation.
+ (instancetype)loadingViewCreate {
    GTLoadingView *loadingView = [GTLoadingView viewLoadFromNibName:kLoadingViewNibName];
    loadingView.backgroundColor = [UIColor colorMainBlue];
    loadingView.alpha = kLoadingViewAlpha;
    loadingView.tag = kLoadingViewTag;
    
    return loadingView;
}

#pragma mark - Tag functions.
+ (NSInteger)loadingViewTag {
    return kLoadingViewTag;
}

@end
