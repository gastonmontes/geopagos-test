//
//  UIImageView+URL.h
//  GeopagosTest
//
//  Created by Gaston  Montes on 22/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^ImageViewURLSuccessBlock)(UIImage *);
typedef void (^ImageViewURLFailBlock)(NSError *);

@interface UIImageView (URL)

- (void)imageViewSetImageFromURL:(NSString *)stringURL success:(ImageViewURLSuccessBlock)success fail:(ImageViewURLFailBlock)fail;

@end

NS_ASSUME_NONNULL_END
