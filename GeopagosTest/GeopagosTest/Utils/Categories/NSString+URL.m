//
//  NSString+URL.m
//  GeopagosTest
//
//  Created by Gaston  Montes on 22/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import "NSString+URL.h"

@implementation NSString (URL)

- (NSString *)stringSecureURL {
    if ([self containsString:@"https"]) {
        return self;
    }
    
    return [self stringByReplacingOccurrencesOfString:@"http" withString:@"https"];
}

@end
