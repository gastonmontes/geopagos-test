//
//  GTBaseViewController+CongratsDelegate.m
//  GeopagosTest
//
//  Created by Gaston  Montes on 24/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import "GTBaseViewController+CongratsDelegate.h"

#import "GTAmountInputViewController.h"

@implementation GTBaseViewController (CongratsDelegate)

#pragma mark - GTCongratsNavigationDelegate implementation.
- (void)congratsWillExit {
    UINavigationController *navigationController = (UINavigationController *)[UIApplication sharedApplication].keyWindow.rootViewController;
    GTAmountInputViewController *amountInputViewController = (GTAmountInputViewController *)navigationController.viewControllers.firstObject;
    [amountInputViewController amountInputResetData];
    
    [self.navigationController popToRootViewControllerAnimated:NO];
}

@end
