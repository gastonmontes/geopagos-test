//
//  UIView+Borders.m
//  GeopagosTest
//
//  Created by Gaston  Montes on 22/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import "UIView+Constraints.h"
#import "UIView+Borders.h"

@implementation UIView (Borders)

#pragma mark - Border functions.
- (void)borderTopWithSize:(CGFloat)borderSize color:(UIColor *)borderColor {
    UIView *bottomBorderView = [[UIView alloc] initWithFrame:CGRectZero];
    bottomBorderView.backgroundColor = borderColor;
    [self addSubview:bottomBorderView];
    
    [bottomBorderView constraintsSizeHeight:borderSize];
    [bottomBorderView constraintsSpacingLeftToSuperView:0];
    [bottomBorderView constraintsSpacingRightToSuperView:0];
    [bottomBorderView constraintsSpacingTopToSuperView:0];
    
}

- (void)borderBottomWithSize:(CGFloat)borderSize color:(UIColor *)borderColor {
    UIView *bottomBorderView = [[UIView alloc] initWithFrame:CGRectZero];
    bottomBorderView.backgroundColor = borderColor;
    [self addSubview:bottomBorderView];
    
    [bottomBorderView constraintsSizeHeight:borderSize];
    [bottomBorderView constraintsSpacingLeftToSuperView:0];
    [bottomBorderView constraintsSpacingRightToSuperView:0];
    [bottomBorderView constraintsSpacingBottomToSuperView:0];
    
}

- (void)borderLeftWithSize:(CGFloat)borderSize color:(UIColor *)borderColor {
    UIView *bottomBorderView = [[UIView alloc] initWithFrame:CGRectZero];
    bottomBorderView.backgroundColor = borderColor;
    [self addSubview:bottomBorderView];
    
    [bottomBorderView constraintsSizeWidth:borderSize];
    [bottomBorderView constraintsSpacingLeftToSuperView:0];
    [bottomBorderView constraintsSpacingTopToSuperView:0];
    [bottomBorderView constraintsSpacingBottomToSuperView:0];
}

- (void)borderRightWithSize:(CGFloat)borderSize color:(UIColor *)borderColor {
    UIView *bottomBorderView = [[UIView alloc] initWithFrame:CGRectZero];
    bottomBorderView.backgroundColor = borderColor;
    [self addSubview:bottomBorderView];
    
    [bottomBorderView constraintsSizeWidth:borderSize];
    [bottomBorderView constraintsSpacingRightToSuperView:0];
    [bottomBorderView constraintsSpacingTopToSuperView:0];
    [bottomBorderView constraintsSpacingBottomToSuperView:0];
}

- (void)borderWithRadius:(CGFloat)cornerRadius size:(CGFloat)borderSize color:(UIColor *)borderColor {
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = cornerRadius;
    self.layer.borderWidth = borderSize;
    self.layer.borderColor = borderColor.CGColor;
}

@end
