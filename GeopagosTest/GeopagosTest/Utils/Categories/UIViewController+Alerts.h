//
//  UIViewController+Alerts.h
//  GeopagosTest
//
//  Created by Gaston  Montes on 22/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^AlertSimpleCallBack)(void);

@interface UIViewController (Alerts)

#pragma mark - Amount alerts.
- (void)alertShowAmountValuesFetchErrorWithRetryCallBack:(AlertSimpleCallBack)retryCallback cancelCallback:(AlertSimpleCallBack)cancelCallback;
- (void)alertShowAmountZeroValueErrorWithCallBack:(AlertSimpleCallBack)okCallback;
- (void)alertShowAmountMinValueErrorWithMinValue:(CGFloat)minValue okCallBack:(AlertSimpleCallBack)okCallback;
- (void)alertShowAmountMaxValueErrorWithMaxValue:(CGFloat)maxValue okCallBack:(AlertSimpleCallBack)okCallback;

#pragma mark - Payment method alerts.
- (void)alertShowPaymentMethodsFetchErrorWithRetryCallBack:(AlertSimpleCallBack)retryCallback cancelCallback:(AlertSimpleCallBack)cancelCallback;

#pragma mark - Card issuer alerts.
- (void)alertShowCardIssuerFetchErrorWithRetryCallback:(AlertSimpleCallBack)retryCallback cancelCallback:(AlertSimpleCallBack)cancelCallback;

#pragma mark - Installments alerts.
- (void)alertShowInstallmentsFetchErrorWithRetryCallback:(AlertSimpleCallBack)retryCallback cancelCallback:(AlertSimpleCallBack)cancelCallback;

@end

NS_ASSUME_NONNULL_END
