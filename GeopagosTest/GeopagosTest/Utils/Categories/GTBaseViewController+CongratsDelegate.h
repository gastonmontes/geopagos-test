//
//  GTBaseViewController+CongratsDelegate.h
//  GeopagosTest
//
//  Created by Gaston  Montes on 24/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import "GTBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface GTBaseViewController (CongratsDelegate)

- (void)congratsWillExit;

@end

NS_ASSUME_NONNULL_END
