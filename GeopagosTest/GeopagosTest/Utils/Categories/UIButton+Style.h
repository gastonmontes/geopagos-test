//
//  UIButton+Style.h
//  GeopagosTest
//
//  Created by Gaston  Montes on 22/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIButton (Style)

- (void)buttonStyleBlue;

@end

NS_ASSUME_NONNULL_END
