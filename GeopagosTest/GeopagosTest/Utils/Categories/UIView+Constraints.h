//
//  UIView+Constraints.h
//  easytiler
//
//  Created by Gaston Montes on 6/16/16.
//  Copyright © 2016 mawape. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Constraints)

// Size constraints.
- (void)constraintsSizeHeight:(CGFloat)viewHeight;
- (void)constraintsSizeWidth:(CGFloat)viewWidth;
- (void)constraintsSizeEqualsAsSuperView;

// Space to margin.
- (void)constraintsSpacingLeftToSuperView:(CGFloat)leftSpace;
- (void)constraintsSpacingTopToSuperView:(CGFloat)topSpace;
- (void)constraintsSpacingRightToSuperView:(CGFloat)rightSpace;
- (void)constraintsSpacingBottomToSuperView:(CGFloat)bottomSpace;
- (void)constraintsSpacingToSuperViewLeft:(CGFloat)leftSpace top:(CGFloat)topSpace right:(CGFloat)rightSpace bottom:(CGFloat)bottomSpace;

@end
