//
//  NSString+Localizable.h
//  GeopagosTest
//
//  Created by Gaston  Montes on 17/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (Localizable)

- (NSString *)stringLocalizable;

@end

NS_ASSUME_NONNULL_END
