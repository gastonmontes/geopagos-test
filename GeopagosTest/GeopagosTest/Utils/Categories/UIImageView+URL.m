//
//  UIImageView+URL.m
//  GeopagosTest
//
//  Created by Gaston  Montes on 22/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import "UIImageView+AFNetworking.h"
#import "UIImageView+URL.h"
#import "NSString+URL.h"

@implementation UIImageView (URL)

- (void)imageViewSetImageFromURL:(NSString *)stringURL success:(ImageViewURLSuccessBlock)success fail:(ImageViewURLFailBlock)fail {
    NSString *stringSecureUrl = [stringURL stringSecureURL];
    NSURL *url = [NSURL URLWithString:stringSecureUrl];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    
    __weak UIImageView *safeSelf = self;
    
    [self setImageWithURLRequest:urlRequest
                placeholderImage:nil
                         success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
                             safeSelf.image = image;
                             success(image);
                         } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
                             fail(error);
                         }];
}

@end
