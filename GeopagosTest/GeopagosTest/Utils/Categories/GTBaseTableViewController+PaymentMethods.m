//
//  GTBaseTableViewController+PaymentMethods.m
//  GeopagosTest
//
//  Created by Gaston  Montes on 23/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import "GTBaseTableViewController+PaymentMethods.h"
#import "NSString+Localizable.h"

#import "GTBaseCellViewModel.h"
#import "GTPaymentMethod.h"

static NSString *const kHeaderTitleCreditCardKey = @"GTPaymentMethodViewController.Header.CreditCard.Title";
static NSString *const kHeaderTitleDebitCardKey = @"GTPaymentMethodViewController.Header.DebitCard.Title";
static NSString *const kHeaderTitleTicketKey = @"GTPaymentMethodViewController.Header.Ticket.Title";
static NSString *const kHeaderTitleOtherKey = @"GTPaymentMethodViewController.Header.Other.Title";

@implementation GTBaseTableViewController (PaymentMethods)

#pragma mark - Data functions.
- (NSString *)sectionTitleForPaymentMethod:(GTPaymentMethod *)paymentMethod {
    switch (paymentMethod.paymentMethodType) {
        case GTPaymentMethodTypeCreditCard:
            return [kHeaderTitleCreditCardKey stringLocalizable];
            break;
        case GTPaymentMethodTypeDebitCard:
            return [kHeaderTitleDebitCardKey stringLocalizable];
            break;
        case GTPaymentMethodTypeTicket:
            return [kHeaderTitleTicketKey stringLocalizable];
            break;
        case GTPaymentMethodTypeUnknown:
            return [kHeaderTitleOtherKey stringLocalizable];
            break;
    }
}

- (NSArray<GTBaseCellViewModel *> *)cellViewModelWithPaymentMethods:(NSArray<GTPaymentMethod *> *)paymentMethods {
    NSMutableArray *cellViewModels = [NSMutableArray arrayWithCapacity:paymentMethods.count];
    
    for (GTPaymentMethod *paymentMethod in paymentMethods) {
        [cellViewModels addObject:[[GTBaseCellViewModel alloc] initWithPaymentMethod:paymentMethod]];
    }
    
    return [NSArray arrayWithArray:cellViewModels];
}

#pragma mark - Add functions.
- (void)tableViewAddSectionWithPaymentMethods:(NSArray<NSArray<GTPaymentMethod *> *> *)paymentMethods {
    for (NSArray<GTPaymentMethod *> *paymentsMethodSection in paymentMethods) {
        GTPaymentMethod *paymentMethod = [paymentsMethodSection firstObject];
        NSString *sectionTitle = [self sectionTitleForPaymentMethod:paymentMethod];
        NSArray <GTBaseCellViewModel *> *cellViewModels = [self cellViewModelWithPaymentMethods:paymentsMethodSection];
        
        [self tableViewAddSectionWithTitle:sectionTitle
                                viewModels:cellViewModels];
    }
}

@end
