//
//  NSString+Localizable.m
//  GeopagosTest
//
//  Created by Gaston  Montes on 17/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import "NSString+Localizable.h"

@implementation NSString (Localizable)

- (NSString *)stringLocalizable {
    return NSLocalizedString(self, @"");
}

@end
