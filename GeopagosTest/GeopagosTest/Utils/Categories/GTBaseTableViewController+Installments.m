//
//  GTBaseTableViewController+Installments.m
//  GeopagosTest
//
//  Created by Gaston  Montes on 24/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import "GTBaseTableViewController+Installments.h"
#import "GTBaseCellViewModel.h"
#import "GTInstallment.h"

@implementation GTBaseTableViewController (Installments)

#pragma mark - Add functions.
- (void)tableViewAddSectionWithInstallments:(NSArray<GTInstallment *> *)installments {
    NSMutableArray *cellViewModels = [NSMutableArray arrayWithCapacity:installments.count];
    
    for (GTInstallment *installment in installments) {
        [cellViewModels addObject:[[GTBaseCellViewModel alloc] initWithInstallment:installment]];
    }
    
    [self tableViewAddSectionWithTitle:@""
                            viewModels:[NSArray arrayWithArray:cellViewModels]];
}

@end
