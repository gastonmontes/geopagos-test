//
//  GTBaseTableViewController+CardIssuer.h
//  GeopagosTest
//
//  Created by Gaston  Montes on 23/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import "GTBaseTableViewController.h"

@class GTCardIssuer;

NS_ASSUME_NONNULL_BEGIN

@interface GTBaseTableViewController (CardIssuer)

- (void)tableViewAddSectionWithCardIssuers:(NSArray<GTCardIssuer *> *)cardIssuers;

@end

NS_ASSUME_NONNULL_END
