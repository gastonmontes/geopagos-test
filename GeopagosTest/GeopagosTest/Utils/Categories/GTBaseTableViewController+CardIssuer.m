//
//  GTBaseTableViewController+CardIssuer.m
//  GeopagosTest
//
//  Created by Gaston  Montes on 23/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import "GTBaseTableViewController+CardIssuer.h"

#import "GTBaseCellViewModel.h"
#import "GTCardIssuer.h"

@implementation GTBaseTableViewController (CardIssuer)

#pragma mark - Add functions.
- (void)tableViewAddSectionWithCardIssuers:(NSArray<GTCardIssuer *> *)cardIssuers {
    NSMutableArray *cellViewModels = [NSMutableArray arrayWithCapacity:cardIssuers.count];
    
    for (GTCardIssuer *cardIssuer in cardIssuers) {
        [cellViewModels addObject:[[GTBaseCellViewModel alloc] initWithCardIssuer:cardIssuer]];
    }
    
    [self tableViewAddSectionWithTitle:@""
                            viewModels:[NSArray arrayWithArray:cellViewModels]];
}

@end
