//
//  GTBaseTableViewController+PaymentMethods.h
//  GeopagosTest
//
//  Created by Gaston  Montes on 23/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import "GTBaseTableViewController.h"

@class GTPaymentMethod;

NS_ASSUME_NONNULL_BEGIN

@interface GTBaseTableViewController (PaymentMethods)

- (void)tableViewAddSectionWithPaymentMethods:(NSArray<NSArray<GTPaymentMethod *> *> *)paymentMethods;

@end

NS_ASSUME_NONNULL_END
