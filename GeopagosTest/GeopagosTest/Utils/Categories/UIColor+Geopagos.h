//
//  UIColor+Geopagos.h
//  GeopagosTest
//
//  Created by Gaston  Montes on 17/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIColor (Geopagos)

+ (UIColor *)colorMainBlue;
+ (UIColor *)colorMainWhite;
+ (UIColor *)colorTextLight;

@end

NS_ASSUME_NONNULL_END
