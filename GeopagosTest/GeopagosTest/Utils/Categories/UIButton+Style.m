//
//  UIButton+Style.m
//  GeopagosTest
//
//  Created by Gaston  Montes on 22/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import "UIColor+Geopagos.h"
#import "UIButton+Style.h"
#import "UIView+Borders.h"

@implementation UIButton (Style)

- (void)buttonStyleBlue {
    [self setTitleColor:[UIColor colorMainWhite] forState:UIControlStateNormal];
    [self setTitleColor:[UIColor colorTextLight] forState:UIControlStateDisabled];
    self.backgroundColor = [UIColor colorMainBlue];
    [self borderWithRadius:4.0 size:1.0 color:[UIColor colorMainBlue]];
    self.titleLabel.font = [UIFont systemFontOfSize:24];
}

@end
