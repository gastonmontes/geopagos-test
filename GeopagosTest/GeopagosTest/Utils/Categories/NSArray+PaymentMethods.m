//
//  NSArray+PaymentMethods.m
//  GeopagosTest
//
//  Created by Gaston  Montes on 22/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import "NSArray+PaymentMethods.h"

#import "GTPaymentMethod.h"

@implementation NSArray (PaymentMethods)

#pragma mark - Utils functions.
- (NSArray<GTPaymentMethod *> *)getFilteredArrayForPaymentType:(GTPaymentMethodType)paymentType {
    return [self filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id  _Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
        GTPaymentMethod *payment = (GTPaymentMethod *)evaluatedObject;
        
        return payment.paymentMethodType == paymentType;
    }]];
}

#pragma mark - Payment method filtering functions.
- (NSArray<GTPaymentMethod *> *)paymentMethodArrayGetCreditCards {
    return [self getFilteredArrayForPaymentType:GTPaymentMethodTypeCreditCard];
}

- (NSArray<GTPaymentMethod *> *)paymentMethodArrayGetDebitCards {
    return [self getFilteredArrayForPaymentType:GTPaymentMethodTypeDebitCard];
}

- (NSArray<GTPaymentMethod *> *)paymentMethodArrayGetTickets {
    return [self getFilteredArrayForPaymentType:GTPaymentMethodTypeTicket];
}

- (NSArray<GTPaymentMethod *> *)paymentMethodArrayGetOthers {
    return [self getFilteredArrayForPaymentType:GTPaymentMethodTypeUnknown];
}

@end
