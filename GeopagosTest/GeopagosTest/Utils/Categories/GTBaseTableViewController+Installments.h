//
//  GTBaseTableViewController+Installments.h
//  GeopagosTest
//
//  Created by Gaston  Montes on 24/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import "GTBaseTableViewController.h"

@class GTInstallment;

NS_ASSUME_NONNULL_BEGIN

@interface GTBaseTableViewController (Installments)

- (void)tableViewAddSectionWithInstallments:(NSArray<GTInstallment *> *)installments;

@end

NS_ASSUME_NONNULL_END
