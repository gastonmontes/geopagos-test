//
//  NSArray+PaymentMethods.h
//  GeopagosTest
//
//  Created by Gaston  Montes on 22/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GTPaymentMethod;

NS_ASSUME_NONNULL_BEGIN

@interface NSArray (PaymentMethods)

- (NSArray<GTPaymentMethod *> *)paymentMethodArrayGetCreditCards;
- (NSArray<GTPaymentMethod *> *)paymentMethodArrayGetDebitCards;
- (NSArray<GTPaymentMethod *> *)paymentMethodArrayGetTickets;
- (NSArray<GTPaymentMethod *> *)paymentMethodArrayGetOthers;

@end

NS_ASSUME_NONNULL_END
