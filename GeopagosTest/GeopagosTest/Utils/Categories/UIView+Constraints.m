//
//  UIView+Constraints.m
//  easytiler
//
//  Created by Gaston Montes on 6/16/16.
//  Copyright © 2016 mawape. All rights reserved.
//

#import "UIView+Constraints.h"

@implementation UIView (Constraints)

#pragma mark - Sizing.
- (void)constraintsSizeHeight:(CGFloat)viewHeight {
    self.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSNumber *height = [NSNumber numberWithFloat:viewHeight];
    [self.superview addConstraints:[NSLayoutConstraint
                                    constraintsWithVisualFormat:@"V:[self(height)]"
                                    options:0
                                    metrics:NSDictionaryOfVariableBindings(height)
                                    views:NSDictionaryOfVariableBindings(self)]];
}

- (void)constraintsSizeWidth:(CGFloat)viewWidth {
    self.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSNumber *width = [NSNumber numberWithFloat:viewWidth];
    [self.superview addConstraints:[NSLayoutConstraint
                                    constraintsWithVisualFormat:@"H:[self(width)]"
                                    options:0
                                    metrics:NSDictionaryOfVariableBindings(width)
                                    views:NSDictionaryOfVariableBindings(self)]];
}

- (void)constraintsSizeEqualsAsSuperView {
    if (self.superview == nil) {
        return;
    }
    
    self.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[self]-0-|"
                                                                           options:0
                                                                           metrics:nil
                                                                             views:NSDictionaryOfVariableBindings(self)]];
    
    [self.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[self]-0-|"
                                                                           options:0
                                                                           metrics:nil
                                                                             views:NSDictionaryOfVariableBindings(self)]];
}

#pragma mark - Spacing.
- (void)constraintsSpacingLeftToSuperView:(CGFloat)leftSpace {
    if (self.superview == nil) {
        return;
    }
    
    self.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSNumber *space = [NSNumber numberWithFloat:leftSpace];
    [self.superview addConstraints:[NSLayoutConstraint
                                    constraintsWithVisualFormat:@"H:|-space-[self]"
                                    options:0
                                    metrics:NSDictionaryOfVariableBindings(space)
                                    views:NSDictionaryOfVariableBindings(self)]];
    
}

- (void)constraintsSpacingTopToSuperView:(CGFloat)topSpace{
    if (self.superview == nil) {
        return;
    }
    
    self.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSNumber *space = [NSNumber numberWithFloat:topSpace];
    [self.superview addConstraints:[NSLayoutConstraint
                                    constraintsWithVisualFormat:@"V:|-space-[self]"
                                    options:0
                                    metrics:NSDictionaryOfVariableBindings(space)
                                    views:NSDictionaryOfVariableBindings(self)]];
}

- (void)constraintsSpacingRightToSuperView:(CGFloat)rightSpace {
    if (self.superview == nil) {
        return;
    }
    
    self.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSNumber *space = [NSNumber numberWithFloat:rightSpace];
    [self.superview addConstraints:[NSLayoutConstraint
                                    constraintsWithVisualFormat:@"H:[self]-space-|"
                                    options:0
                                    metrics:NSDictionaryOfVariableBindings(space)
                                    views:NSDictionaryOfVariableBindings(self)]];
}

- (void)constraintsSpacingBottomToSuperView:(CGFloat)bottomSpace {
    if (self.superview == nil) {
        return;
    }
    
    self.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSNumber *space = [NSNumber numberWithFloat:bottomSpace];
    [self.superview addConstraints:[NSLayoutConstraint
                                    constraintsWithVisualFormat:@"V:[self]-space-|"
                                    options:0
                                    metrics:NSDictionaryOfVariableBindings(space)
                                    views:NSDictionaryOfVariableBindings(self)]];
}

- (void)constraintsSpacingToSuperViewLeft:(CGFloat)leftSpace top:(CGFloat)topSpace right:(CGFloat)rightSpace bottom:(CGFloat)bottomSpace {
    [self constraintsSpacingLeftToSuperView:leftSpace];
    [self constraintsSpacingTopToSuperView:topSpace];
    [self constraintsSpacingRightToSuperView:rightSpace];
    [self constraintsSpacingBottomToSuperView:bottomSpace];
}

@end
