//
//  UIViewController+Alerts.m
//  GeopagosTest
//
//  Created by Gaston  Montes on 22/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import "UIViewController+Alerts.h"
#import "NSString+Localizable.h"

static NSString *const kAlertErrorTitleKey = @"UIViewController+Alerts.Error.Title.Key";

static NSString *const kAlertErrorActionCancelKey = @"UIViewController+Alerts.Error.Action.Cancel.Title.Key";
static NSString *const kAlertErrorActionRetryKey = @"UIViewController+Alerts.Error.Action.Retry.Title.Key";
static NSString *const kAlertErrorActionOKKey = @"UIViewController+Alerts.Error.Action.OK.Title.Key";

static NSString *const kAlertErrorMessageAmountValuesFetchErrorKey = @"UIViewController+Alerts.Error.Message.Amount.Values.Fetch.Title.Key";
static NSString *const kAlertErrorMessageAmountValuesZeroErrorKey = @"UIViewController+Alerts.Error.Message.Amount.Values.Zero.Title.Key";
static NSString *const kAlertErrorMessageAmountValuesMinErrorKey = @"UIViewController+Alerts.Error.Message.Amount.Values.Min.Title.Key";
static NSString *const kAlertErrorMessageAmountValuesMaxErrorKey = @"UIViewController+Alerts.Error.Message.Amount.Values.Max.Title.Key";

static NSString *const kAlertErrorMessagePaymentMethodsFetchErrorKey = @"UIViewController+Alerts.Error.Message.PaymentMethod.Fetch.Title.Key";

static NSString *const kAlertErrorMessageCardIssuerFetchErrorKey = @"UIViewController+Alerts.Error.Message.CardIssuer.Fetch.Title.Key";

static NSString *const kAlertErrorMessageInstallmentsFetchErrorKey = @"UIViewController+Alerts.Error.Message.Installments.Fetch.Title.Key";

@implementation UIViewController (Alerts)

#pragma mark - Utils functions.
- (void)presentAlert:(NSString *)title message:(NSString *)message actions:(NSArray<UIAlertAction *> *)actions {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title
                                                                             message:message
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    for (UIAlertAction *action in actions) {
        [alertController addAction:action];
    }
    
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - Amount alerts.
- (void)alertShowAmountValuesFetchErrorWithRetryCallBack:(AlertSimpleCallBack)retryCallback cancelCallback:(AlertSimpleCallBack)cancelCallback {
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:[kAlertErrorActionCancelKey stringLocalizable]
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction *retry) {
                                                             cancelCallback();
                                                         }];
    
    UIAlertAction *retryAction = [UIAlertAction actionWithTitle:[kAlertErrorActionRetryKey stringLocalizable]
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction *retry) {
                                                            retryCallback();
                                                        }];
    
    
    [self presentAlert:[kAlertErrorTitleKey stringLocalizable]
               message:[kAlertErrorMessageAmountValuesFetchErrorKey stringLocalizable]
               actions:@[cancelAction, retryAction]];
}

- (void)alertShowAmountZeroValueErrorWithCallBack:(AlertSimpleCallBack)okCallback {
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:[kAlertErrorActionOKKey stringLocalizable]
                                                       style:UIAlertActionStyleCancel
                                                     handler:^(UIAlertAction *retry) {
                                                         okCallback();
                                                     }];
    
    [self presentAlert:[kAlertErrorTitleKey stringLocalizable]
               message:[kAlertErrorMessageAmountValuesZeroErrorKey stringLocalizable]
               actions:@[okAction]];
}

- (void)alertShowAmountMinValueErrorWithMinValue:(CGFloat)minValue okCallBack:(AlertSimpleCallBack)okCallback {
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:[kAlertErrorActionOKKey stringLocalizable]
                                                       style:UIAlertActionStyleCancel
                                                     handler:^(UIAlertAction *retry) {
                                                         okCallback();
                                                     }];
    
    [self presentAlert:[kAlertErrorTitleKey stringLocalizable]
               message:[NSString stringWithFormat:[kAlertErrorMessageAmountValuesMinErrorKey stringLocalizable], minValue]
               actions:@[okAction]];
}

- (void)alertShowAmountMaxValueErrorWithMaxValue:(CGFloat)maxValue okCallBack:(AlertSimpleCallBack)okCallback {
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:[kAlertErrorActionOKKey stringLocalizable]
                                                       style:UIAlertActionStyleCancel
                                                     handler:^(UIAlertAction *retry) {
                                                         okCallback();
                                                     }];
    
    [self presentAlert:[kAlertErrorTitleKey stringLocalizable]
               message:[NSString stringWithFormat:[kAlertErrorMessageAmountValuesMaxErrorKey stringLocalizable], maxValue]
               actions:@[okAction]];
}

#pragma mark - Payment method alerts.
- (void)alertShowPaymentMethodsFetchErrorWithRetryCallBack:(AlertSimpleCallBack)retryCallback cancelCallback:(AlertSimpleCallBack)cancelCallback {
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:[kAlertErrorActionCancelKey stringLocalizable]
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction *retry) {
                                                             cancelCallback();
                                                         }];
    
    UIAlertAction *retryAction = [UIAlertAction actionWithTitle:[kAlertErrorActionRetryKey stringLocalizable]
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction *retry) {
                                                            retryCallback();
                                                        }];
    
    
    [self presentAlert:[kAlertErrorTitleKey stringLocalizable]
               message:[kAlertErrorMessagePaymentMethodsFetchErrorKey stringLocalizable]
               actions:@[cancelAction, retryAction]];
}

#pragma mark - Card issuer alerts.
- (void)alertShowCardIssuerFetchErrorWithRetryCallback:(AlertSimpleCallBack)retryCallback cancelCallback:(AlertSimpleCallBack)cancelCallback {
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:[kAlertErrorActionCancelKey stringLocalizable]
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction *retry) {
                                                             cancelCallback();
                                                         }];
    
    UIAlertAction *retryAction = [UIAlertAction actionWithTitle:[kAlertErrorActionRetryKey stringLocalizable]
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction *retry) {
                                                            retryCallback();
                                                        }];
    
    
    [self presentAlert:[kAlertErrorTitleKey stringLocalizable]
               message:[kAlertErrorMessageCardIssuerFetchErrorKey stringLocalizable]
               actions:@[cancelAction, retryAction]];
}

#pragma mark - Installments alerts.
- (void)alertShowInstallmentsFetchErrorWithRetryCallback:(AlertSimpleCallBack)retryCallback cancelCallback:(AlertSimpleCallBack)cancelCallback {
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:[kAlertErrorActionCancelKey stringLocalizable]
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction *retry) {
                                                             cancelCallback();
                                                         }];
    
    UIAlertAction *retryAction = [UIAlertAction actionWithTitle:[kAlertErrorActionRetryKey stringLocalizable]
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction *retry) {
                                                            retryCallback();
                                                        }];
    
    
    [self presentAlert:[kAlertErrorTitleKey stringLocalizable]
               message:[kAlertErrorMessageInstallmentsFetchErrorKey stringLocalizable]
               actions:@[cancelAction, retryAction]];
}

@end
