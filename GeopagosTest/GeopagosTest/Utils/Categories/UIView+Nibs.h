//
//  UIView+Nibs.h
//  GeopagosTest
//
//  Created by Gaston  Montes on 17/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIView (Nibs)

+ (instancetype)viewLoadFromNibName:(NSString *)nibName;

@end

NS_ASSUME_NONNULL_END
