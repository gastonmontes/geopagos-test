//
//  UIView+Borders.h
//  GeopagosTest
//
//  Created by Gaston  Montes on 22/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIView (Borders)

- (void)borderTopWithSize:(CGFloat)borderSize color:(UIColor *)borderColor;
- (void)borderBottomWithSize:(CGFloat)borderSize color:(UIColor *)borderColor;
- (void)borderLeftWithSize:(CGFloat)borderSize color:(UIColor *)borderColor;
- (void)borderRightWithSize:(CGFloat)borderSize color:(UIColor *)borderColor;
- (void)borderWithRadius:(CGFloat)cornerRadius size:(CGFloat)borderSize color:(UIColor *)borderColor;

@end

NS_ASSUME_NONNULL_END
