//
//  UIView+Nibs.m
//  GeopagosTest
//
//  Created by Gaston  Montes on 17/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import "UIView+Nibs.h"

@implementation UIView (Nibs)

+ (instancetype)viewLoadFromNibName:(NSString *)nibName {
    return [[[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil] objectAtIndex:0];
}

@end
