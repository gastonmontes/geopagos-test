//
//  UIColor+Geopagos.m
//  GeopagosTest
//
//  Created by Gaston  Montes on 17/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import "UIColor+Geopagos.h"

@implementation UIColor (Geopagos)

#pragma mark - Utils functions.
+ (UIColor *)colorWithRed:(NSInteger)red green:(NSInteger)green blue:(NSInteger)blue {
    return [UIColor colorWithRed: red / 255.0 green: green / 255.0 blue: blue / 255.0 alpha:1.0];
}

#pragma mark - Pallete's colors.
+ (UIColor *)colorMainBlue {
    return [UIColor colorWithRed:36 green:86 blue:197];
}

+ (UIColor *)colorMainWhite {
    return [UIColor whiteColor];
}

+ (UIColor *)colorTextLight {
    return [UIColor lightGrayColor];
}

@end
