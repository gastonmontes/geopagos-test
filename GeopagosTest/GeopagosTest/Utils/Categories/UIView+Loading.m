//
//  UIView+Loading.m
//  GeopagosTest
//
//  Created by Gaston  Montes on 17/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import "UIView+Constraints.h"
#import "UIView+Loading.h"

#import "GTLoadingView.h"

@implementation UIView (Loading)

#pragma mark - Show/hide functions.
- (void)loadingShow {
    if ([self getLoadingView] == nil) {
        GTLoadingView *loadingView = [GTLoadingView loadingViewCreate];
        [self addSubview:loadingView];
        
        [loadingView constraintsSizeEqualsAsSuperView];
    }
}

- (void)loadingHide {
    GTLoadingView *loadingView = [self getLoadingView];
    
    if (loadingView != nil) {
        [loadingView removeFromSuperview];
    }
}

#pragma mark - Loading view functions.
- (GTLoadingView *)getLoadingView {
    return [self viewWithTag:[GTLoadingView loadingViewTag]];
}

@end
