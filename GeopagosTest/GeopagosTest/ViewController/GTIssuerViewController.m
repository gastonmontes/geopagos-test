//
//  GTIssuerViewController.m
//  GeopagosTest
//
//  Created by Gaston  Montes on 23/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import "GTInstallmentsViewController.h"
#import "GTCongratsViewController.h"
#import "GTIssuerViewController.h"
#import "GTBaseCellViewModel.h"
#import "GTCardIssuerService.h"
#import "GTPaymentMethod.h"
#import "GTInstallment.h"
#import "GTCardIssuer.h"
#import "GTBaseCell.h"

#import "GTBaseViewController+CongratsDelegate.h"
#import "GTBaseTableViewController+CardIssuer.h"
#import "UIViewController+Alerts.h"
#import "UIColor+Geopagos.h"
#import "UIView+Loading.h"

static NSString *const kInstallmentsSegueIdentifier = @"cardIssuerToInstallmentsSegue";
static NSString *const kCongratsSegueIdentifier = @"cardIssuerToCongratsSegue";

@interface GTIssuerViewController () <GTCongratsNavigationDelegate>

@property (nonatomic, assign) CGFloat paymentAmount;
@property (nonatomic, strong) GTPaymentMethod *paymentMethod;
@property (nonatomic, copy) NSArray<GTCardIssuer *> *cardIssuerList;

@end

@implementation GTIssuerViewController

#pragma mark - View life cycle.
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.cardIssuerList = nil;
    [self tableViewResetData];
    
    [self fetchCardIssuers];
}

#pragma mark - Setters.
- (void)setSelectedAmount:(CGFloat)amount {
    self.paymentAmount = amount;
}

- (void)setSelectedPaymentMethod:(GTPaymentMethod *)paymentMethod {
    self.paymentMethod = paymentMethod;
}

#pragma mark - Data functions.
- (void)fetchCardIssuers {
    [self.view loadingShow];
    [self fetchDataHideRetryButton];
    
    __weak GTIssuerViewController *safeSelf = self;
    
    [GTCardIssuerService serviceGetCardIssuerForPaymentMethodID:self.paymentMethod.paymentMethodID
                                                        success:^(NSArray<GTCardIssuer *> *cardIssuers) {
                                                            safeSelf.cardIssuerList = cardIssuers;
                                                            
                                                            if (cardIssuers.count > 0) {
                                                                [safeSelf tableViewAddSectionWithCardIssuers:cardIssuers];
                                                            } else {
                                                                [safeSelf pushCongratsWithSelectedCardIssuer];
                                                            }

                                                            [safeSelf.view loadingHide];
                                                        } fail:^(NSError *error) {
                                                            safeSelf.cardIssuerList = nil;
                                                            
                                                            [safeSelf alertShowCardIssuerFetchErrorWithRetryCallback:^{
                                                                [safeSelf fetchCardIssuers];
                                                            } cancelCallback:^{
                                                                [safeSelf fetchDataShowRetryButton];
                                                                
                                                                [safeSelf tableViewResetData];
                                                                
                                                                [safeSelf.view loadingHide];
                                                            }];
                                                        }];
}

#pragma mark - GTBaseViewControllerRetryFetchDataProtocol implementation.
- (void)fetchDataRetryAction {
    [self fetchCardIssuers];
}

#pragma mark - GTBaseTableViewProtocol implementation.
- (void)tableViewDidSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    GTCardIssuer *cardIssuer = [self.cardIssuerList objectAtIndex:indexPath.row];
    
    [self pushInstallmentsSelectionWithSelectedCardIssuer:cardIssuer];
}

#pragma mark - Segue functions.
- (void)pushInstallmentsSelectionWithSelectedCardIssuer:(GTCardIssuer *)cardIssuer {
    [self performSegueWithIdentifier:kInstallmentsSegueIdentifier sender:cardIssuer];
}

- (void)pushCongratsWithSelectedCardIssuer {
    [self performSegueWithIdentifier:kCongratsSegueIdentifier sender:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:kInstallmentsSegueIdentifier]) {
        GTInstallmentsViewController *installmentsSelection = (GTInstallmentsViewController *)segue.destinationViewController;
        GTCardIssuer *cardIssuer = (GTCardIssuer *)sender;
        
        [installmentsSelection setSelectedAmount:self.paymentAmount];
        [installmentsSelection setSelectedPaymentMethod:self.paymentMethod];
        [installmentsSelection setSelectedCardIssue:cardIssuer];
    } else if ([segue.identifier isEqualToString:kCongratsSegueIdentifier]) {
        GTCongratsViewController *congratsViewController = (GTCongratsViewController *)segue.destinationViewController;
        GTPaymentMethod *paymentMethod = self.paymentMethod;
        GTCardIssuer *cardIssuer = [[GTCardIssuer alloc] initWithPaymentMethod:self.paymentMethod];
        GTInstallment *installment = [[GTInstallment alloc] initWithAmount:self.paymentAmount];
        
        [congratsViewController setCongratsDelegate:self];
        [congratsViewController setSelectedAmount:self.paymentAmount];
        [congratsViewController setSelectedPaymentMethod:paymentMethod];
        [congratsViewController setSelectedCardIssue:cardIssuer];
        [congratsViewController setSelectedInstallments:installment];
    }
}

@end
