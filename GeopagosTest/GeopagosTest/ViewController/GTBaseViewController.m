//
//  GTBaseViewController.m
//  GeopagosTest
//
//  Created by Gaston  Montes on 17/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import "GTBaseViewController.h"

#import "UIColor+Geopagos.h"
#import "NSString+Localizable.h"

static NSString *const kRetryButtonTitleKey = @"UIViewController+Alerts.Error.Action.Retry.Title.Key";

@interface GTBaseViewController ()

@end

@implementation GTBaseViewController

#pragma mark - View life cycle.
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupControllerStyle];
    [self setupStatusBarStyle];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self setViewControllerTitleText];
}

#pragma mark - Setter functions.
- (void)setViewControllerTitleText {
    NSString *viewControllerTitle = [[NSString stringWithFormat:@"%@.title", NSStringFromClass([self class])] stringLocalizable];
    self.navigationController.navigationBar.topItem.title = viewControllerTitle;
    self.navigationItem.title = viewControllerTitle;
    self.title = viewControllerTitle;
}

#pragma mark - Controller styling.
- (void)setupControllerStyle {
    self.view.backgroundColor = [UIColor colorMainWhite];
    
    [self setViewControllerTitleText];
}

#pragma mark - Status bar styling.
- (void)setupStatusBarStyle {
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    
    self.navigationController.navigationBar.tintColor = [UIColor colorMainWhite];
    self.navigationController.navigationBar.backItem.title = @"";
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorMainBlue];
}

#pragma mark - GTBaseViewControllerRetryFetchDataProtocol implementation.
- (void)fetchDataShowRetryButton {
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[kRetryButtonTitleKey stringLocalizable]
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(fetchDataRetryAction)];
}

- (void)fetchDataHideRetryButton {
    self.navigationItem.rightBarButtonItem = nil;
}

- (void)fetchDataRetryAction {
    [self doesNotRecognizeSelector:_cmd];
}

@end
