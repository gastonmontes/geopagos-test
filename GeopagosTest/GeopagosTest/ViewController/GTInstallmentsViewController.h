//
//  GTInstallmentsViewController.h
//  GeopagosTest
//
//  Created by Gaston  Montes on 23/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import "GTBaseTableViewController.h"

@class GTPaymentMethod;
@class GTCardIssuer;

NS_ASSUME_NONNULL_BEGIN

@interface GTInstallmentsViewController : GTBaseTableViewController

- (void)setSelectedAmount:(CGFloat)amount;
- (void)setSelectedPaymentMethod:(GTPaymentMethod *)paymentMethod;
- (void)setSelectedCardIssue:(GTCardIssuer *)cardIssuer;

@end

NS_ASSUME_NONNULL_END
