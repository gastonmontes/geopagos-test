//
//  GTAmountInputViewController.m
//  GeopagosTest
//
//  Created by Gaston  Montes on 17/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import "GTPaymentMethodViewController.h"
#import "GTAmountInputViewController.h"
#import "GTPaymentMethodService.h"

#import "UIViewController+Alerts.h"
#import "NSString+Localizable.h"
#import "UIColor+Geopagos.h"
#import "UIView+Loading.h"
#import "UIButton+Style.h"

static NSString *const kAmountMinValueFormatKey = @"GTAmountInputViewController.Amount.Values.Minimum";
static NSString *const kAmountMaxValueFormatKey = @"GTAmountInputViewController.Amount.Values.Maximum";
static NSString *const kContinueButtonTitleKey = @"GTAmountInputViewController.Button.Continue.Title.Key";

static NSString *const kPaymentMethodSelectionSegueID = @"amountToPaymentMethodSegue";

@interface GTAmountInputViewController () <UITextFieldDelegate>

@property (nonatomic, assign) CGFloat amountMinValue;
@property (nonatomic, assign) CGFloat amountMaxValue;

@property (nonatomic, weak) IBOutlet UITextField *amountTextField;
@property (nonatomic, weak) IBOutlet UIButton *amountHideTextFieldButton;
@property (nonatomic, weak) IBOutlet UILabel *amountMinValueLabel;
@property (nonatomic, weak) IBOutlet UILabel *amountMaxValueLabel;
@property (nonatomic, weak) IBOutlet UIButton *amountContinueButton;

@end

@implementation GTAmountInputViewController

#pragma mark - View life cycle.
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupAmountTextFieldStyle];
    [self setupMinAndMaxAmountLabels];
    [self setupContinueButton];
    
    [self fetchMinimumAndMaximumAmountValues];
}

#pragma mark - Styling functions.
- (void)setupAmountTextFieldStyle {
    self.amountTextField.textColor = [UIColor colorMainBlue];
}

- (void)setupMinAndMaxAmountLabels {
    self.amountMinValueLabel.text = @"";
    self.amountMinValueLabel.textColor = [UIColor colorTextLight];
    
    self.amountMaxValueLabel.text = @"";
    self.amountMaxValueLabel.textColor = [UIColor colorTextLight];
}

- (void)setupContinueButton {
    [self.amountContinueButton buttonStyleBlue];
    [self.amountContinueButton setTitle:[kContinueButtonTitleKey stringLocalizable] forState:UIControlStateNormal];
}

#pragma mark - UITextFieldDelegate implementation.
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSCharacterSet *numbersAndDotOnly = [NSCharacterSet characterSetWithCharactersInString:@"0123456789."];
    NSCharacterSet *characterSetFromNewString = [NSCharacterSet characterSetWithCharactersInString:string];
    
    if ([numbersAndDotOnly isSupersetOfSet:characterSetFromNewString] == NO) {
        return NO;
    }
    if (textField.text.length == 0) {
        textField.text = @"$";
    }
    
    if ([textField.text containsString:@"."] && [string isEqualToString:@"."]) {
        return NO;
    }
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    self.amountHideTextFieldButton.hidden = NO;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    self.amountHideTextFieldButton.hidden = YES;
    
    CGFloat value = [self getAmountValue];
    
    if (value > 0) {
        textField.text = [NSString stringWithFormat:@"$%.02f", value];
    } else {
        textField.text = @"";
    }
}

#pragma mark - Actions functions.
- (IBAction)hideAmountTextFieldKeyboard:(id)sender {
    [self.amountTextField resignFirstResponder];
}

- (IBAction)continueAction:(id)sender {
    [self.amountTextField resignFirstResponder];
    
    CGFloat value = [self getAmountValue];
    
    __weak GTAmountInputViewController *safeSelf = self;
    
    if (value == 0) {
        [self alertShowAmountZeroValueErrorWithCallBack:^{
            [safeSelf.amountTextField becomeFirstResponder];
        }];
    } else if (value < self.amountMinValue) {
        [self alertShowAmountMinValueErrorWithMinValue:self.amountMinValue okCallBack:^{
            [safeSelf.amountTextField becomeFirstResponder];
        }];
    } else if (value > self.amountMaxValue) {
        [self alertShowAmountMaxValueErrorWithMaxValue:self.amountMaxValue okCallBack:^{
            [safeSelf.amountTextField becomeFirstResponder];
        }];
    } else {
        [self pushPaymentMethodSelection];
    }
}

#pragma mark - Data functions.
- (void)fetchMinimumAndMaximumAmountValues {
    [self.view loadingShow];
    
    [self fetchDataHideRetryButton];
    
    __weak GTAmountInputViewController *safeSelf = self;
    
    [GTPaymentMethodService serviceGetPaymentMethodMinAndMaxAmountWithSuccess:^(CGFloat minimumAmount, CGFloat maximumAmount) {
        safeSelf.amountContinueButton.enabled = YES;
        
        safeSelf.amountMinValue = minimumAmount;
        safeSelf.amountMaxValue = maximumAmount;
        
        safeSelf.amountMinValueLabel.text = [NSString stringWithFormat:[kAmountMinValueFormatKey stringLocalizable], floorf(minimumAmount)];
        safeSelf.amountMaxValueLabel.text = [NSString stringWithFormat:[kAmountMaxValueFormatKey stringLocalizable], floorf(maximumAmount)];
        
        [safeSelf.view loadingHide];
    } fail:^(NSError *error) {
        [safeSelf alertShowAmountValuesFetchErrorWithRetryCallBack:^{
            [safeSelf fetchMinimumAndMaximumAmountValues];
        } cancelCallback:^{
            [safeSelf fetchDataShowRetryButton];
            [safeSelf.view loadingHide];
        }];
    }];
}

- (CGFloat)getAmountValue {
    NSString *valueString = [self.amountTextField.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
    CGFloat value = [valueString floatValue];
    
    return value;
}

- (void)amountInputResetData {
    self.amountTextField.text = @"";
    [self fetchMinimumAndMaximumAmountValues];
    
    self.title = [@"GTAmountInputViewController.title" stringLocalizable];
}

#pragma mark - Segue functions.
- (void)pushPaymentMethodSelection {
    [self performSegueWithIdentifier:kPaymentMethodSelectionSegueID sender:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:kPaymentMethodSelectionSegueID]) {
        GTPaymentMethodViewController *paymentMethod = segue.destinationViewController;
        [paymentMethod setSelectedAmount:[self getAmountValue]];
    }
}

#pragma mark - GTBaseViewControllerRetryFetchDataProtocol implementation.
- (void)fetchDataRetryAction {
    [self fetchMinimumAndMaximumAmountValues];
}

@end
