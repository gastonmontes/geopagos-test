//
//  GTCongratsViewController.h
//  GeopagosTest
//
//  Created by Gaston  Montes on 24/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import "GTBaseViewController.h"

@class GTPaymentMethod;
@class GTInstallment;
@class GTCardIssuer;

NS_ASSUME_NONNULL_BEGIN

@protocol GTCongratsNavigationDelegate <NSObject>

- (void)congratsWillExit;

@end

@interface GTCongratsViewController : GTBaseViewController

- (void)setCongratsDelegate:(id <GTCongratsNavigationDelegate>)congratDelegate;
- (void)setSelectedAmount:(CGFloat)amount;
- (void)setSelectedPaymentMethod:(GTPaymentMethod *)paymentMethod;
- (void)setSelectedCardIssue:(GTCardIssuer *)cardIssuer;
- (void)setSelectedInstallments:(GTInstallment *)installmens;

@end

NS_ASSUME_NONNULL_END
