//
//  GTBaseTableViewController.h
//  GeopagosTest
//
//  Created by Gaston  Montes on 23/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import "GTBaseViewController.h"

@class GTBaseCellViewModel;

NS_ASSUME_NONNULL_BEGIN

@protocol GTBaseTableViewProtocol <NSObject>

@optional
- (void)tableViewDidSelectRowAtIndexPath:(NSIndexPath *)indexPath;

@end

@interface GTBaseTableViewController : GTBaseViewController

- (void)tableViewResetData;
- (void)tableViewAddSectionWithTitle:(NSString *)sectionTitle viewModels:(NSArray<GTBaseCellViewModel *> *)cellViewModels;

@end

NS_ASSUME_NONNULL_END
