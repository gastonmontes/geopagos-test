//
//  GTPaymentMethodViewController.h
//  GeopagosTest
//
//  Created by Gaston  Montes on 22/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import "GTBaseTableViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface GTPaymentMethodViewController : GTBaseTableViewController

- (void)setSelectedAmount:(CGFloat)paymentAmount;

@end

NS_ASSUME_NONNULL_END
