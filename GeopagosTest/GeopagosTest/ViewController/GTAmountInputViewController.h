//
//  GTAmountInputViewController.h
//  GeopagosTest
//
//  Created by Gaston  Montes on 17/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GTBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface GTAmountInputViewController : GTBaseViewController

- (void)amountInputResetData;

@end

NS_ASSUME_NONNULL_END
