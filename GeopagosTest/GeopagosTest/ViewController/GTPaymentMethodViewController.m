//
//  GTPaymentMethodViewController.m
//  GeopagosTest
//
//  Created by Gaston  Montes on 22/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import "GTPaymentMethodViewController.h"
#import "GTCongratsViewController.h"
#import "GTIssuerViewController.h"
#import "GTPaymentMethodService.h"
#import "GTBaseSectionHeader.h"
#import "GTBaseCellViewModel.h"
#import "GTPaymentMethod.h"
#import "GTInstallment.h"
#import "GTCardIssuer.h"
#import "GTBaseCell.h"

#import "GTBaseTableViewController+PaymentMethods.h"
#import "GTBaseViewController+CongratsDelegate.h"
#import "UIViewController+Alerts.h"
#import "NSArray+PaymentMethods.h"
#import "NSString+Localizable.h"
#import "UIColor+Geopagos.h"
#import "UIView+Loading.h"

static NSString *const kIssuerSelectionSegueIdentifier = @"paymentMethodToCardIssuerSegue";
static NSString *const kCongratsSegueIdentifier =  @"paymentMethodToCongratsSegue";

@interface GTPaymentMethodViewController () <GTCongratsNavigationDelegate>

@property (nonatomic, assign) CGFloat paymentAmountValue;
@property (nonatomic, copy) NSArray<NSArray<GTPaymentMethod *> *> *paymentMethods;

@end

@implementation GTPaymentMethodViewController

#pragma mark - View life cycle.
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.paymentMethods = nil;
    [self tableViewResetData];
    
    [self fetchPaymentMethods];
}

#pragma mark - Setter functions.
- (void)setSelectedAmount:(CGFloat)paymentAmount {
    self.paymentAmountValue = paymentAmount;
}

#pragma mark - Data functions.
- (void)processPaymentMethods:(NSArray<GTPaymentMethod *> *)paymentMethods {
    NSArray<GTPaymentMethod *> *paymentCreditCards = [paymentMethods paymentMethodArrayGetCreditCards];
    NSArray<GTPaymentMethod *> *paymentDebitCards = [paymentMethods paymentMethodArrayGetDebitCards];
    NSArray<GTPaymentMethod *> *paymentTickets = [paymentMethods paymentMethodArrayGetTickets];
    NSArray<GTPaymentMethod *> *paymentOther = [paymentMethods paymentMethodArrayGetOthers];
    
    NSMutableArray<NSArray<GTPaymentMethod *> *> *payments = [NSMutableArray arrayWithCapacity:4];
    
    if (paymentCreditCards.count > 0) { [payments addObject:paymentCreditCards]; }
    if (paymentDebitCards.count > 0) { [payments addObject:paymentDebitCards]; }
    if (paymentTickets.count > 0) { [payments addObject:paymentTickets]; }
    if (paymentOther.count > 0) { [payments addObject:paymentOther]; }
    
    self.paymentMethods = [NSArray arrayWithArray:payments];
}

- (void)fetchPaymentMethods {
    [self.view loadingShow];
    
    [self fetchDataHideRetryButton];
    
    __weak GTPaymentMethodViewController *safeSelf = self;
    
    [GTPaymentMethodService serviceGetPaymentMethodForAmount:self.paymentAmountValue success:^(NSArray<GTPaymentMethod *> *paymentMethods) {
        [safeSelf processPaymentMethods:paymentMethods];
        
        [safeSelf tableViewAddSectionWithPaymentMethods:safeSelf.paymentMethods];
        
        [safeSelf.view loadingHide];
    } fail:^(NSError *error) {
        safeSelf.paymentMethods = nil;
        
        [safeSelf alertShowPaymentMethodsFetchErrorWithRetryCallBack:^{
            [safeSelf fetchPaymentMethods];
        } cancelCallback:^{
            [safeSelf fetchDataShowRetryButton];
            
            [safeSelf tableViewResetData];
            
            [safeSelf.view loadingHide];
        }];
    }];
}

#pragma mark - GTBaseViewControllerRetryFetchDataProtocol implementation.
- (void)fetchDataRetryAction {
    [self fetchPaymentMethods];
}

#pragma mark - GTBaseTableViewProtocol implementation.
- (void)tableViewDidSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray<GTPaymentMethod *> *paymentSectionList = [self.paymentMethods objectAtIndex:indexPath.section];
    GTPaymentMethod *paymentMethod = [paymentSectionList objectAtIndex:indexPath.row];
    
    switch (paymentMethod.paymentMethodType) {
        case GTPaymentMethodTypeCreditCard:
            [self pushToIssuerSelectionWithSelectedPaymentMethod:paymentMethod];
            break;
        default:
            [self pushToCongratsWithPaymentMethod:paymentMethod];
            break;
    }
}

#pragma mark - Segue functions.
- (void)pushToIssuerSelectionWithSelectedPaymentMethod:(GTPaymentMethod *)paymentMethod {
    [self performSegueWithIdentifier:kIssuerSelectionSegueIdentifier sender:paymentMethod];
}

- (void)pushToCongratsWithPaymentMethod:(GTPaymentMethod *)paymentMethod {
    [self performSegueWithIdentifier:kCongratsSegueIdentifier sender:paymentMethod];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:kIssuerSelectionSegueIdentifier]) {
        GTIssuerViewController *issuerSelection = (GTIssuerViewController *)segue.destinationViewController;
        GTPaymentMethod *paymentMethod = (GTPaymentMethod *)sender;
        
        [issuerSelection setSelectedAmount:self.paymentAmountValue];
        [issuerSelection setSelectedPaymentMethod:paymentMethod];
    } else if ([segue.identifier isEqualToString:kCongratsSegueIdentifier]) {
        GTCongratsViewController *congratsViewController = (GTCongratsViewController *)segue.destinationViewController;
        GTPaymentMethod *paymentMethod = (GTPaymentMethod *)sender;
        GTCardIssuer *cardIssuer = [[GTCardIssuer alloc] initWithPaymentMethod:paymentMethod];
        GTInstallment *installment = [[GTInstallment alloc] initWithAmount:self.paymentAmountValue];
        
        [congratsViewController setCongratsDelegate:self];
        [congratsViewController setSelectedAmount:self.paymentAmountValue];
        [congratsViewController setSelectedPaymentMethod:paymentMethod];
        [congratsViewController setSelectedCardIssue:cardIssuer];
        [congratsViewController setSelectedInstallments:installment];
    }
}

@end
