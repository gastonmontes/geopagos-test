//
//  GTIssuerViewController.h
//  GeopagosTest
//
//  Created by Gaston  Montes on 23/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import "GTBaseTableViewController.h"

@class GTPaymentMethod;

NS_ASSUME_NONNULL_BEGIN

@interface GTIssuerViewController : GTBaseTableViewController

- (void)setSelectedAmount:(CGFloat)amount;
- (void)setSelectedPaymentMethod:(GTPaymentMethod *)paymentMethod;

@end

NS_ASSUME_NONNULL_END
