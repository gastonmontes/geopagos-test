//
//  GTInstallmentsViewController.m
//  GeopagosTest
//
//  Created by Gaston  Montes on 23/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import "GTInstallmentsViewController.h"
#import "GTCongratsViewController.h"
#import "GTInstallmentsService.h"
#import "GTPaymentMethod.h"
#import "GTCardIssuer.h"

#import "GTBaseTableViewController+Installments.h"
#import "GTBaseViewController+CongratsDelegate.h"
#import "UIViewController+Alerts.h"
#import "UIView+Loading.h"

static NSString *const kCongratsSegueIdentifier = @"installmentsToCongratsSegue";

@interface GTInstallmentsViewController () <GTCongratsNavigationDelegate>

@property (nonatomic, assign) CGFloat installmentAmount;
@property (nonatomic, strong) GTPaymentMethod *installmentPaymentMethod;
@property (nonatomic, strong) GTCardIssuer *installmentCardIssuer;
@property (nonatomic, copy) NSArray <GTInstallment *> *installmentsList;

@end

@implementation GTInstallmentsViewController

#pragma mark - View life cycle.
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.installmentsList = nil;
    [self tableViewResetData];
    
    [self fetchInstallments];
}

#pragma mark - Setter functions.
- (void)setSelectedAmount:(CGFloat)amount {
    self.installmentAmount = amount;
}

- (void)setSelectedPaymentMethod:(GTPaymentMethod *)paymentMethod {
    self.installmentPaymentMethod = paymentMethod;
}

- (void)setSelectedCardIssue:(GTCardIssuer *)cardIssuer {
    self.installmentCardIssuer = cardIssuer;
}

#pragma mark - Data functions.
- (void)fetchInstallments {
    [self.view loadingShow];
    [self fetchDataHideRetryButton];
    
    __weak GTInstallmentsViewController *safeSelf = self;
    
    [GTInstallmentsService serviceGetInstallmentsForAmount:self.installmentAmount
                                           paymentMethodId:self.installmentPaymentMethod.paymentMethodID
                                              cardIssuerID:self.installmentCardIssuer.cardIssuerID
                                                   success:^(NSArray<GTInstallment *> * installments) {
                                                       safeSelf.installmentsList = installments;
                                                       
                                                       [safeSelf tableViewAddSectionWithInstallments:installments];
                                                       
                                                       [safeSelf.view loadingHide];
                                                   } fail:^(NSError * error) {
                                                       safeSelf.installmentsList = nil;
                                                       
                                                       [safeSelf alertShowInstallmentsFetchErrorWithRetryCallback:^{
                                                           [safeSelf fetchInstallments];
                                                       } cancelCallback:^{
                                                           [safeSelf fetchDataShowRetryButton];
                                                           
                                                           [safeSelf tableViewResetData];
                                                           
                                                           [safeSelf.view loadingHide];
                                                       }];
                                                   }];
}

#pragma mark - GTBaseViewControllerRetryFetchDataProtocol implementation.
- (void)fetchDataRetryAction {
    [self fetchInstallments];
}

#pragma mark - GTBaseTableViewProtocol implementation.
- (void)tableViewDidSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    GTInstallment *installment = [self.installmentsList objectAtIndex:indexPath.row];
    
    [self pushCongratsWithSelectedInstallment:installment];
}

#pragma mark - Segue functions.
- (void)pushCongratsWithSelectedInstallment:(GTInstallment *)installment {
    [self performSegueWithIdentifier:kCongratsSegueIdentifier sender:installment];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:kCongratsSegueIdentifier]) {
        GTCongratsViewController *congratsViewController = (GTCongratsViewController *)segue.destinationViewController;
        GTInstallment *installments = (GTInstallment *)sender;
        
        [congratsViewController setCongratsDelegate:self];
        [congratsViewController setSelectedAmount:self.installmentAmount];
        [congratsViewController setSelectedPaymentMethod:self.installmentPaymentMethod];
        [congratsViewController setSelectedCardIssue:self.installmentCardIssuer];
        [congratsViewController setSelectedInstallments:installments];
    }
}

@end
