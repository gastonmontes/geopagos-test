//
//  GTCongratsViewController.m
//  GeopagosTest
//
//  Created by Gaston  Montes on 24/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import "GTCongratsViewController.h"
#import "GTPaymentMethod.h"
#import "GTInstallment.h"
#import "GTCardIssuer.h"

#import "NSString+Localizable.h"
#import "UIColor+Geopagos.h"
#import "UIImageView+URL.h"

static NSString *const kInfoAmountText = @"GTCongratsViewController.Label.Info.Amount.Text";
static NSString *const kInfoPaymentMethodText = @"GTCongratsViewController.Label.Info.PaymentMethod.Text";
static NSString *const kInfoCardIssuerText = @"GTCongratsViewController.Label.Info.CardIssuer.Text";
static NSString *const kInfoInstallmentsText = @"GTCongratsViewController.Label.Info.Installments.Text";

@interface GTCongratsViewController ()

@property (nonatomic, weak) id <GTCongratsNavigationDelegate> navigationDelegate;

@property (nonatomic, assign) CGFloat congratsAmount;
@property (nonatomic, strong) GTPaymentMethod *congratsPaymentMethod;
@property (nonatomic, strong) GTCardIssuer *congratsCardIssuer;
@property (nonatomic, strong) GTInstallment *congratsInstallments;

@property (nonatomic, weak) IBOutlet UILabel *amountSelectedInfo;
@property (nonatomic, weak) IBOutlet UILabel *amountSelectedValue;
@property (nonatomic, weak) IBOutlet UILabel *paymentMethodSelectedInfo;
@property (nonatomic, weak) IBOutlet UIImageView *paymentMethodSelectedImageView;
@property (nonatomic, weak) IBOutlet UILabel *paymentMethodSelectedValue;
@property (nonatomic, weak) IBOutlet UILabel *cardIssuerSelectedInfo;
@property (nonatomic, weak) IBOutlet UIImageView *cardIssuerSelectedImageView;
@property (nonatomic, weak) IBOutlet UILabel *cardIssuerSelectedValue;
@property (nonatomic, weak) IBOutlet UILabel *installmentsSelectedInfo;
@property (nonatomic, weak) IBOutlet UILabel *installmentsSelectedValue;

@end

@implementation GTCongratsViewController

#pragma mark - View life cycle.
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorMainBlue];
    
    [self setupAmountComponents];
    [self setupPaymentMethodComponents];
    [self setupCardIssuerComponents];
    [self setupInstallmentsComponents];
}

#pragma mark - Setup functions.
- (void)setupAmountComponents {
    self.amountSelectedInfo.textColor = [UIColor colorMainWhite];
    self.amountSelectedValue.textColor = [UIColor colorMainWhite];
    
    self.amountSelectedInfo.text = [kInfoAmountText stringLocalizable];
    self.amountSelectedValue.text = [NSString stringWithFormat:@"$%.02f", self.congratsAmount];
}

- (void)setupPaymentMethodComponents {
    self.paymentMethodSelectedInfo.textColor = [UIColor colorMainWhite];
    self.paymentMethodSelectedValue.textColor = [UIColor colorMainWhite];
    
    self.paymentMethodSelectedInfo.text = [kInfoPaymentMethodText stringLocalizable];
    self.paymentMethodSelectedValue.text = self.congratsPaymentMethod.paymentMethodName;
    [self.paymentMethodSelectedImageView imageViewSetImageFromURL:self.congratsPaymentMethod.paymentMethodImage
                                                          success:^(UIImage *image) {}
                                                             fail:^(NSError *error) {}];
}

- (void)setupCardIssuerComponents {
    self.cardIssuerSelectedInfo.textColor = [UIColor colorMainWhite];
    self.cardIssuerSelectedValue.textColor = [UIColor colorMainWhite];
    
    self.cardIssuerSelectedInfo.text = [kInfoCardIssuerText stringLocalizable];
    self.cardIssuerSelectedValue.text = self.congratsCardIssuer.cardIssuerName;
    [self.cardIssuerSelectedImageView imageViewSetImageFromURL:self.congratsCardIssuer.cardIssuerImageName
                                                       success:^(UIImage *image) {}
                                                          fail:^(NSError *error) {}];
}

- (void)setupInstallmentsComponents {
    self.installmentsSelectedInfo.textColor = [UIColor colorMainWhite];
    self.installmentsSelectedValue.textColor = [UIColor colorMainWhite];
    
    self.installmentsSelectedInfo.text = [kInfoInstallmentsText stringLocalizable];
    self.installmentsSelectedValue.text = self.congratsInstallments.installmentMessage;
}

#pragma mark - Setter functions.
- (void)setCongratsDelegate:(id <GTCongratsNavigationDelegate>)congratDelegate {
    self.navigationDelegate = congratDelegate;
}

- (void)setSelectedAmount:(CGFloat)amount {
    self.congratsAmount = amount;
}

- (void)setSelectedPaymentMethod:(GTPaymentMethod *)paymentMethod {
    self.congratsPaymentMethod = paymentMethod;
}

- (void)setSelectedCardIssue:(GTCardIssuer *)cardIssuer {
    self.congratsCardIssuer = cardIssuer;
}

- (void)setSelectedInstallments:(GTInstallment *)installmens {
    self.congratsInstallments = installmens;
}

#pragma mark - Action functions.
- (IBAction)closeAction:(id)sender {
    if (self.navigationDelegate != nil) {
        [self.navigationDelegate congratsWillExit];
    }
    
    [self dismissViewControllerAnimated:true completion:nil];
}

@end
