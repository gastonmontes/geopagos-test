//
//  GTBaseTableViewController.m
//  GeopagosTest
//
//  Created by Gaston  Montes on 23/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import "GTAmountInputViewController.h"
#import "GTBaseTableViewController.h"
#import "GTBaseSectionHeader.h"
#import "GTBaseCell.h"

#import "UIColor+Geopagos.h"

@interface GTBaseTableViewController () <UITableViewDelegate, UITableViewDataSource, GTBaseTableViewProtocol>

@property (nonatomic, copy) NSArray<NSString *> *sectionTitles;
@property (nonatomic, copy) NSArray<NSArray<GTBaseCellViewModel *> *> *cellViewModels;

@property (nonatomic, weak) id <GTBaseTableViewProtocol> tableViewDelegate;

@property (nonatomic, weak) IBOutlet UITableView *baseTableView;

@end

@implementation GTBaseTableViewController

#pragma mark - View life cycle.
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.cellViewModels = nil;
    self.tableViewDelegate = self;
    
    [self setupTableViewProperties];
}

#pragma mark - Data functions
- (void)tableViewResetData {
    self.sectionTitles = nil;
    self.cellViewModels = nil;
    
    [self.baseTableView reloadData];
}

- (void)tableViewAddSectionWithTitle:(NSString *)sectionTitle viewModels:(NSArray<GTBaseCellViewModel *> *)cellViewModels {
    NSMutableArray *titles = [NSMutableArray arrayWithArray:self.sectionTitles];
    [titles addObject:sectionTitle];
    self.sectionTitles = [NSArray arrayWithArray:titles];
    
    NSMutableArray *viewModels = [NSMutableArray arrayWithArray:self.cellViewModels];
    [viewModels addObject:cellViewModels];
    self.cellViewModels = [NSArray arrayWithArray:viewModels];
    
    [self.baseTableView reloadData];
}

#pragma mark - Table view setup functions.
- (void)setupTableViewProperties {
    self.baseTableView.backgroundColor = [UIColor colorMainWhite];
    
    [self.baseTableView registerNib:[UINib nibWithNibName:[GTBaseCell cellNibName] bundle:[NSBundle mainBundle]]
             forCellReuseIdentifier:[GTBaseCell cellIdentifier]];
}

#pragma mark - UITableViewDataSource implementation.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.cellViewModels.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray<GTBaseCellViewModel *> *paymentSectionList = [self.cellViewModels objectAtIndex:section];
    return paymentSectionList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    GTBaseCell *cell = [tableView dequeueReusableCellWithIdentifier:[GTBaseCell cellIdentifier]
                                                       forIndexPath:indexPath];
    
    NSArray<GTBaseCellViewModel *> *cellViewModelSectionList = [self.cellViewModels objectAtIndex:indexPath.section];
    GTBaseCellViewModel *cellViewModel = [cellViewModelSectionList objectAtIndex:indexPath.row];
    [cell cellSetViewModel:cellViewModel];
    
    return cell;
}

#pragma mark - UITableViewDelegate implementation.
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *sectionTitle = [self.sectionTitles objectAtIndex:section];
    
    if (sectionTitle.length > 0) {
        return [GTBaseSectionHeader createHeaderWithTitle:sectionTitle];
    }
    
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    NSString *sectionTitle = [self.sectionTitles objectAtIndex:section];
    
    if (sectionTitle.length > 0) {
        return [GTBaseSectionHeader headerHeight];
    }
    
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self respondsToSelector:@selector(tableViewDidSelectRowAtIndexPath:)]) {
        [self.tableViewDelegate tableViewDidSelectRowAtIndexPath:indexPath];
    }
}

@end
