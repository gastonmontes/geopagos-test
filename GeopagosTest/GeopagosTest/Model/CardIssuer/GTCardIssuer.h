//
//  GTCardIssuer.h
//  GeopagosTest
//
//  Created by Gaston  Montes on 23/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GTPaymentMethod;

NS_ASSUME_NONNULL_BEGIN

@interface GTCardIssuer : NSObject

@property (readonly, nonatomic, copy) NSString *cardIssuerID;
@property (readonly, nonatomic, copy) NSString *cardIssuerName;
@property (readonly, nonatomic, copy) NSString *cardIssuerImageName;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;
- (instancetype)initWithPaymentMethod:(GTPaymentMethod *)paymentMethod;

@end

NS_ASSUME_NONNULL_END
