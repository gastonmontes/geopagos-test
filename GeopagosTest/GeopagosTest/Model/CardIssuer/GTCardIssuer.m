//
//  GTCardIssuer.m
//  GeopagosTest
//
//  Created by Gaston  Montes on 23/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import "GTPaymentMethod.h"
#import "GTCardIssuer.h"

@interface GTCardIssuer ()

@property (nonatomic, copy) NSString *cardIssuerID;
@property (nonatomic, copy) NSString *cardIssuerName;
@property (nonatomic, copy) NSString *cardIssuerImageName;

@end

@implementation GTCardIssuer

#pragma mark - Initialization functions.
- (instancetype)initWithDictionary:(NSDictionary *)dictionary {
    self = [super init];
    
    if (self != nil) {
        _cardIssuerID = [dictionary objectForKey:@"id"];
        _cardIssuerName = [dictionary objectForKey:@"name"];
        _cardIssuerImageName = [dictionary objectForKey:@"thumbnail"];
    }
    
    return self;
}

- (instancetype)initWithPaymentMethod:(GTPaymentMethod *)paymentMethod {
    self = [super init];
    
    if (self != nil) {
        _cardIssuerID = paymentMethod.paymentMethodID;
        _cardIssuerName = paymentMethod.paymentMethodName;
        _cardIssuerImageName = paymentMethod.paymentMethodImage;
    }
    
    return self;
}

@end
