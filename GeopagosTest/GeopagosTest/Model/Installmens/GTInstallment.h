//
//  GTInstallment.h
//  GeopagosTest
//
//  Created by Gaston  Montes on 23/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GTInstallment : NSObject

@property (readonly, nonatomic, copy) NSString *installmentMessage;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;
- (instancetype)initWithAmount:(CGFloat)amount;

@end

NS_ASSUME_NONNULL_END
