//
//  GTInstallment.m
//  GeopagosTest
//
//  Created by Gaston  Montes on 23/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import "GTInstallment.h"

@interface GTInstallment ()

@property (nonatomic, copy) NSString *installmentMessage;

@end

@implementation GTInstallment

#pragma mark - Initialization functions.
- (instancetype)initWithDictionary:(NSDictionary *)dictionary {
    self = [super init];
    
    if (self != nil) {
        _installmentMessage = [dictionary objectForKey:@"recommended_message"];
    }
    
    return self;
}

- (instancetype)initWithAmount:(CGFloat)amount {
    self = [super init];
    
    if (self != nil) {
        _installmentMessage = [NSString stringWithFormat:@"1 cuota de $%.02f ($%.02f)", amount, amount];
    }
    
    return self;
}

@end
