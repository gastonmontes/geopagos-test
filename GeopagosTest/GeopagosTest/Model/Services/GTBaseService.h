//
//  GTBaseService.h
//  GeopagosTest
//
//  Created by Gaston  Montes on 23/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

extern NSString *const kMercadoPagoBaseURL;
extern NSString *const kMercadoPagoPublicKey;

extern NSString *const kMercadoPagoParamPublicKey;
extern NSString *const kMercadoPagoParamPaymentMethodId;
extern NSString *const kMercadoPagoParamAmountKey;
extern NSString *const kMercadoPagoParamCardIssuerKey;

typedef void (^GTBaseFailBlock)(NSError *);

@interface GTBaseService : NSObject

@end

NS_ASSUME_NONNULL_END
