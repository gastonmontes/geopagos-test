//
//  GTInstallmentsService.h
//  GeopagosTest
//
//  Created by Gaston  Montes on 23/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GTBaseService.h"

@class GTInstallment;

NS_ASSUME_NONNULL_BEGIN

typedef void (^GTInstallmentsSuccessBlock)(NSArray<GTInstallment *> *);

@interface GTInstallmentsService : GTBaseService

+ (void)serviceGetInstallmentsForAmount:(CGFloat)amount
                        paymentMethodId:(NSString *)paymentMethodID
                           cardIssuerID:(NSString *)cardIssuerID
                                success:(GTInstallmentsSuccessBlock)success
                                   fail:(GTBaseFailBlock)fail;

@end

NS_ASSUME_NONNULL_END
