//
//  GTCardIssuerService.m
//  GeopagosTest
//
//  Created by Gaston  Montes on 23/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import "GTCardIssuerService.h"
#import "GTCardIssuer.h"
#import "AFNetworking.h"

@implementation GTCardIssuerService

#pragma mark - Get card issuer functions.
+ (void)serviceGetCardIssuerForPaymentMethodID:(NSString *)paymentMethodID success:(GTCardIssuerSuccessBlock)success fail:(GTBaseFailBlock)fail {
    NSString *urlString = [NSString stringWithFormat:@"%@/card_issuers", kMercadoPagoBaseURL];
    NSURL *url = [NSURL URLWithString:urlString];
    AFHTTPSessionManager *manager   = [AFHTTPSessionManager manager];
    NSDictionary *paramsDict = @{ kMercadoPagoParamPublicKey : kMercadoPagoPublicKey, kMercadoPagoParamPaymentMethodId : paymentMethodID };
    
    [manager GET:url.absoluteString parameters:paramsDict progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSMutableArray *cardIssuers = [NSMutableArray arrayWithCapacity:0];
        
        for (NSDictionary *cardIssuerDict in responseObject) {
            GTCardIssuer *cardIssuer = [[GTCardIssuer alloc] initWithDictionary:cardIssuerDict];
            [cardIssuers addObject:cardIssuer];
        }
        
        success([NSArray arrayWithArray:cardIssuers]);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        fail(error);
    }];
}

@end
