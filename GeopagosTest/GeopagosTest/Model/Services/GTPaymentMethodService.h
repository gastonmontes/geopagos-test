//
//  GTPaymentMethodService.h
//  GeopagosTest
//
//  Created by Gaston  Montes on 23/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "GTBaseService.h"

@class GTPaymentMethod;

NS_ASSUME_NONNULL_BEGIN

typedef void (^GTPaymentMethodAmountMinMaxSuccessBlock)(CGFloat, CGFloat);
typedef void (^GTPaymentMethodGetSuccessBlock)(NSArray<GTPaymentMethod *> *);

@interface GTPaymentMethodService : GTBaseService

+ (void)serviceGetPaymentMethodMinAndMaxAmountWithSuccess:(GTPaymentMethodAmountMinMaxSuccessBlock)success fail:(GTBaseFailBlock)fail;

+ (void)serviceGetPaymentMethodForAmount:(CGFloat)amountValue success:(GTPaymentMethodGetSuccessBlock)success fail:(GTBaseFailBlock)fail;

@end

NS_ASSUME_NONNULL_END
