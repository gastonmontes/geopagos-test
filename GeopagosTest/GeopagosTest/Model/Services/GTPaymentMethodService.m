//
//  GTPaymentMethodService.m
//  GeopagosTest
//
//  Created by Gaston  Montes on 23/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import "GTPaymentMethodService.h"
#import "GTPaymentMethod.h"
#import "AFNetworking.h"

@implementation GTPaymentMethodService

#pragma mark - Amount functions.
+ (void)serviceGetPaymentMethodMinAndMaxAmountWithSuccess:(GTPaymentMethodAmountMinMaxSuccessBlock)success fail:(GTBaseFailBlock)fail {
    NSURL *url = [NSURL URLWithString:kMercadoPagoBaseURL];
    AFHTTPSessionManager *manager   = [AFHTTPSessionManager manager];
    
    [manager GET:url.absoluteString parameters:@{ kMercadoPagoParamPublicKey : kMercadoPagoPublicKey } progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        CGFloat minimumAmount = CGFLOAT_MAX;
        CGFloat maximumAmount = 0;
        
        for (NSDictionary *paymentMethod in responseObject) {
            CGFloat maxAmmount = [[paymentMethod objectForKey:@"max_allowed_amount"] doubleValue];
            CGFloat minAmmount = [[paymentMethod objectForKey:@"min_allowed_amount"] doubleValue];
            
            if (minimumAmount > minAmmount) {
                minimumAmount = minAmmount;
            }
            
            if (maximumAmount < maxAmmount) {
                maximumAmount = maxAmmount;
            }
        }
        
        success(minimumAmount, maximumAmount);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        fail(error);
    }];
}

#pragma mark - Payment method functions.
+ (void)serviceGetPaymentMethodForAmount:(CGFloat)amountValue success:(GTPaymentMethodGetSuccessBlock)success fail:(GTBaseFailBlock)fail {
    NSURL *url = [NSURL URLWithString:kMercadoPagoBaseURL];
    AFHTTPSessionManager *manager   = [AFHTTPSessionManager manager];
    
    [manager GET:url.absoluteString parameters:@{ kMercadoPagoParamPublicKey : kMercadoPagoPublicKey } progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSMutableArray *paymentMethods = [NSMutableArray arrayWithCapacity:0];
        
        for (NSDictionary *paymentMethodDict in responseObject) {
            GTPaymentMethod *paymentMethod = [[GTPaymentMethod alloc] initWithDictionary:paymentMethodDict];
            
            if (amountValue >= paymentMethod.paymentMethodMinAmount && amountValue <= paymentMethod.paymentMethodMaxAmount) {
                [paymentMethods addObject:paymentMethod];
            }
        }
        
        success([NSArray arrayWithArray:paymentMethods]);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        fail(error);
    }];
}

@end
