//
//  GTInstallmentsService.m
//  GeopagosTest
//
//  Created by Gaston  Montes on 23/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import "GTInstallmentsService.h"
#import "GTInstallment.h"
#import "AFNetworking.h"

@implementation GTInstallmentsService

#pragma mark - Fetch installments functions.
+ (void)serviceGetInstallmentsForAmount:(CGFloat)amount
                        paymentMethodId:(NSString *)paymentMethodID
                           cardIssuerID:(NSString *)cardIssuerID
                                success:(GTInstallmentsSuccessBlock)success
                                   fail:(GTBaseFailBlock)fail {
    NSString *urlString = [NSString stringWithFormat:@"%@/installments", kMercadoPagoBaseURL];
    NSURL *url = [NSURL URLWithString:urlString];
    AFHTTPSessionManager *manager   = [AFHTTPSessionManager manager];
    
    NSDictionary *paramsDict = @{ kMercadoPagoParamPublicKey : kMercadoPagoPublicKey,
                                  kMercadoPagoParamPaymentMethodId : paymentMethodID,
                                  kMercadoPagoParamAmountKey : [NSNumber numberWithFloat:amount],
                                  kMercadoPagoParamCardIssuerKey : cardIssuerID };
    
    [manager GET:url.absoluteString parameters:paramsDict progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSMutableArray *installments = [NSMutableArray arrayWithCapacity:0];
        NSArray *responseArray = (NSArray *)responseObject;
        
        if (responseArray.count > 0) {
            NSDictionary *installmentsObject = responseArray.firstObject;
            NSArray *installmentsList = [installmentsObject objectForKey:@"payer_costs"];
            
            for (NSDictionary *installmentDict in installmentsList) {
                GTInstallment *installment = [[GTInstallment alloc] initWithDictionary:installmentDict];
                [installments addObject:installment];
            }
        }
       
        success([NSArray arrayWithArray:installments]);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        fail(error);
    }];
}

@end
