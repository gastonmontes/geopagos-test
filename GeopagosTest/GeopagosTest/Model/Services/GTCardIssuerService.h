//
//  GTCardIssuerService.h
//  GeopagosTest
//
//  Created by Gaston  Montes on 23/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GTBaseService.h"

@class GTCardIssuer;

NS_ASSUME_NONNULL_BEGIN

typedef void (^GTCardIssuerSuccessBlock)(NSArray<GTCardIssuer *> *);

@interface GTCardIssuerService : GTBaseService

+ (void)serviceGetCardIssuerForPaymentMethodID:(NSString *)paymentMethodID success:(GTCardIssuerSuccessBlock)success fail:(GTBaseFailBlock)fail;

@end

NS_ASSUME_NONNULL_END
