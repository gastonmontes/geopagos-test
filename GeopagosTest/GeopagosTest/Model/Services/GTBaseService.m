//
//  GTBaseService.m
//  GeopagosTest
//
//  Created by Gaston  Montes on 23/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import "GTBaseService.h"

NSString *const kMercadoPagoBaseURL = @"https://api.mercadopago.com/v1/payment_methods";
NSString *const kMercadoPagoPublicKey = @"444a9ef5-8a6b-429f-abdf-587639155d88";

NSString *const kMercadoPagoParamPublicKey = @"public_key";
NSString *const kMercadoPagoParamPaymentMethodId = @"payment_method_id";
NSString *const kMercadoPagoParamAmountKey = @"amount";
NSString *const kMercadoPagoParamCardIssuerKey = @"issuer.id";

@implementation GTBaseService

@end
