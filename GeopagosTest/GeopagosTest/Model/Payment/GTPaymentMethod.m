//
//  GTPaymentMethod.m
//  GeopagosTest
//
//  Created by Gaston  Montes on 22/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GTPaymentMethod.h"

@interface GTPaymentMethod ()

@property (nonatomic, copy) NSString *paymentMethodID;
@property (nonatomic, copy) NSString *paymentMethodName;
@property (nonatomic, copy) NSString *paymentMethodImage;
@property (nonatomic, assign) CGFloat paymentMethodMinAmount;
@property (nonatomic, assign) CGFloat paymentMethodMaxAmount;
@property (nonatomic, assign) GTPaymentMethodType paymentMethodType;
@property (nonatomic, assign) GTPaymentMethodStatus paymentMethodStatus;

@end

@implementation GTPaymentMethod

#pragma mark - Initialization functions.
- (instancetype)initWithDictionary:(NSDictionary *)dictionary {
    self = [super init];
    
    if (self != nil) {
        _paymentMethodID = [dictionary objectForKey:@"id"];
        _paymentMethodMinAmount = [[dictionary objectForKey:@"min_allowed_amount"] floatValue];
        _paymentMethodMaxAmount = [[dictionary objectForKey:@"max_allowed_amount"] floatValue];
        _paymentMethodName = [dictionary objectForKey:@"name"];
        _paymentMethodType = [self paymentMethodTypeFromString:[dictionary objectForKey:@"payment_type_id"]];
        _paymentMethodImage = [dictionary objectForKey:@"thumbnail"];
        _paymentMethodStatus = [self paymentMethodStatusFromString:[dictionary objectForKey:@"status"]];
    }
    
    return self;
}

#pragma mark - String to enum functions.
- (GTPaymentMethodType)paymentMethodTypeFromString:(NSString *)paymentMethodType {
    NSDictionary *paymentMethodTypesDict = @{ @"credit_card": @0, @"debit_card": @1, @"ticket": @2 };
    
    if ([paymentMethodTypesDict objectForKey:paymentMethodType] != nil) {
        return [[paymentMethodTypesDict objectForKey:paymentMethodType] integerValue];
    }
    
    return GTPaymentMethodTypeUnknown;
}

- (GTPaymentMethodStatus)paymentMethodStatusFromString:(NSString *)paymentMethodStatus {
    NSDictionary *paymentMethodTypesDict = @{ @"active": @0 };
    
    if ([paymentMethodTypesDict objectForKey:paymentMethodStatus] != nil) {
        return [[paymentMethodTypesDict objectForKey:paymentMethodStatus] integerValue];
    }
    
    return GTPaymentMethodStatusUnknown;
}

@end
