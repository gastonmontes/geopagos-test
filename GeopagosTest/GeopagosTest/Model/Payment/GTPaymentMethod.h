//
//  GTPaymentMethod.h
//  GeopagosTest
//
//  Created by Gaston  Montes on 22/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, GTPaymentMethodType) {
    GTPaymentMethodTypeCreditCard = 0,
    GTPaymentMethodTypeDebitCard,
    GTPaymentMethodTypeTicket,
    GTPaymentMethodTypeUnknown
};

typedef NS_ENUM(NSInteger, GTPaymentMethodStatus) {
    GTPaymentMethodStatusActive = 0,
    GTPaymentMethodStatusUnknown
};

@interface GTPaymentMethod : NSObject

@property (readonly, nonatomic, copy) NSString *paymentMethodID;
@property (readonly, nonatomic, copy) NSString *paymentMethodName;
@property (readonly, nonatomic, copy) NSString *paymentMethodImage;
@property (readonly, nonatomic, assign) CGFloat paymentMethodMinAmount;
@property (readonly, nonatomic, assign) CGFloat paymentMethodMaxAmount;
@property (readonly, nonatomic, assign) GTPaymentMethodType paymentMethodType;
@property (readonly, nonatomic, assign) GTPaymentMethodStatus paymentMethodStatus;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end

NS_ASSUME_NONNULL_END
