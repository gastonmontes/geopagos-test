//
//  GTPaymentMethodCellViewModel.h
//  GeopagosTest
//
//  Created by Gaston  Montes on 22/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GTPaymentMethod;
@class GTInstallment;
@class GTCardIssuer;

NS_ASSUME_NONNULL_BEGIN

@interface GTBaseCellViewModel : NSObject

@property (readonly, nonatomic, copy) NSString *paymentMethodCellName;
@property (readonly, nonatomic, copy) NSString *paymentMethodCellImageURLString;

- (instancetype)initWithPaymentMethod:(GTPaymentMethod *)paymentMethod;
- (instancetype)initWithCardIssuer:(GTCardIssuer *)cardIssuer;
- (instancetype)initWithInstallment:(GTInstallment *)installment;

@end

NS_ASSUME_NONNULL_END
