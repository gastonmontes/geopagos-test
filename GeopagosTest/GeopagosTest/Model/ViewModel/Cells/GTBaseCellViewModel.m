//
//  GTPaymentMethodCellViewModel.m
//  GeopagosTest
//
//  Created by Gaston  Montes on 22/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

#import "GTBaseCellViewModel.h"
#import "GTPaymentMethod.h"
#import "GTInstallment.h"
#import "GTCardIssuer.h"

@interface GTBaseCellViewModel ()

@property (nonatomic, copy) NSString *paymentMethodCellName;
@property (nonatomic, copy) NSString *paymentMethodCellImageURLString;

@end

@implementation GTBaseCellViewModel

#pragma mark - Initialization.
- (instancetype)initWithPaymentMethod:(GTPaymentMethod *)paymentMethod {
    self = [super init];
    
    if (self != nil) {
        _paymentMethodCellName = paymentMethod.paymentMethodName;
        _paymentMethodCellImageURLString = paymentMethod.paymentMethodImage;
    }
    
    return self;
}

- (instancetype)initWithCardIssuer:(GTCardIssuer *)cardIssuer {
    self = [super init];
    
    if (self != nil) {
        _paymentMethodCellName = cardIssuer.cardIssuerName;
        _paymentMethodCellImageURLString = cardIssuer.cardIssuerImageName;
    }
    
    return self;
}

- (instancetype)initWithInstallment:(GTInstallment *)installment {
    self = [super init];
    
    if (self != nil) {
        _paymentMethodCellName = installment.installmentMessage;
        _paymentMethodCellImageURLString = @"";
    }
    
    return self;
}

@end
