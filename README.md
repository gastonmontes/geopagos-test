# Geopagos-Test

This repository was created to complete the Geopagos test exercise.

## Requirements

|   Geotest version.  |     Minimum iOS Target      |.              Notes              |
|:-------------------:|:---------------------------:|:--------------------------------:|
|        1.0.0        |           iOS 12.0          |     Xcode 10 is recommended.     |

## Get Started

### Install CocoaPods
[CocoaPods](http://cocoapods.org) is a dependency manager for iOS, which automates and simplifies the process of using 3rd-party libraries in your projects.

CocoaPods is distributed as a ruby gem, and is installed by running the following commands in Terminal.app:

```ruby
$ sudo gem install cocoapods
```

### First option (Recommended)

1) Download the 'GeopagosSetUp.command' file.

2) Copy this file to the folder where you want to have the code.

3) Run it with a double click.

If you can't run the command because of an error of permissions:

1) Open the Terminal and go to the folder where you have the 'GeopagosSetUp.command' file.

2) Run:

```ruby
chmod 755 GeopagosSetUp.command
chmod +x GeopagosSetUp.command
```

That's all!

### Second option

This option consist in doing the automated commands of the bash script of the first option yourself.

1) Clone the repository:

```ruby
git clone https://gastonmontes@bitbucket.org/gastonmontes/geopagos-test.git --verbose
```

2) Go to the project's folder:

```ruby
cd geopagos-test
cd GeopagosTest
```

3) Checkout master branch:

```ruby
git checkout master
```

4) Run pod install:

```ruby
pod install --verbose
```

5) Open the workspace with Xcode:

```ruby
open GeopagosTest.xcworkspace
```

